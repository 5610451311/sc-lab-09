import java.util.ArrayList;
import java.util.Collections;


public class Testperson {

	public static void main(String[] args) {
		
		
		ArrayList<Person> listp = new ArrayList<Person>();
		listp.add(new Person("wut", 180000));
		listp.add(new Person("got", 160000));
		listp.add(new Person("nick",109000));

		System.out.println("---before sort by income");
		for (Person c : listp) 
			System.out.println(c);

		Collections.sort(listp);
		
		System.out.println("\n---after sort by income");
		for (Person c : listp) 
			System.out.println(c);
		
	}
}