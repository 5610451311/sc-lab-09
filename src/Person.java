

public class Person implements Taxable,Comparable<Person> {
	
	private String name; 
	private int income;
	public Person(String name,int income){
		this.name = name;
		this.income = income;
	}

	public double getTax() {
		if(income<=300000)return income*0.05;
		return (300000*0.05)+((income-300000)*0.1);
	}
	
	public String toString(){
		return name+" income: "+income;
	}
	
	@Override
	public int compareTo(Person other) {
		if (this.income < other.income ) { return -1; }
		if (this.income > other.income ) { return 1;  }
		return 0;
	}

}
