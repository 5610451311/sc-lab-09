import java.util.ArrayList;
import java.util.Collections;

public class Testcompany {

	public static void main(String[] args) {
		
		
		ArrayList<Company> outcome = new ArrayList<Company>();
		Company outcome1 = new Company("A", 10444, 50000);
		Company outcome2 = new Company("B", 23000, 7000);
		Company outcome3 = new Company("C", 300000, 400000);
		outcome.add(outcome1);
		outcome.add(outcome2);
		outcome.add(outcome3);
		
		System.out.println("\n Before sort with Outcome : ");		
		for (Company pBefore : outcome) {			
		System.out.println(pBefore.getOutcome());
		}
		
		Collections.sort(outcome, new ExpenseComparator());
		System.out.println("\n---- After sort with Outcome : ");
		for (Company pAfter : outcome) {
		System.out.println(pAfter.getOutcome());		
		}
		
		ArrayList<Company> profit = new ArrayList<Company>();
		Company profit1 = new Company("A", 10444, 50000);
		Company profit2 = new Company("B", 23000, 7000);
		Company profit3 = new Company("C", 300000, 00000);
		outcome.add(profit1);
		outcome.add(profit2);
		outcome.add(profit3);
		
		System.out.println("\n Before sort with Profit : ");		
		for (Company pBefore : profit) {			
		System.out.println(pBefore.getProfit());
		}
		
		Collections.sort(profit, new ProfitComparator());
		System.out.println("\n---- After sort with Profit : ");
		for (Company pAfter : profit) {
		System.out.println(pAfter.getProfit());		
		}
	}
	
}
		