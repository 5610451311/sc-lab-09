import java.util.ArrayList;
import java.util.Collections;


public class Testproduct {

	public static void main(String[] args) {
		
		
		ArrayList<Product> listp = new ArrayList<Product>();
		listp.add(new Product("wut", 1804680));
		listp.add(new Product("got", 1494411));
		listp.add(new Product("nick",1097040));

		System.out.println("---before sort by income");
		for (Product c : listp) 
			System.out.println(c);

		Collections.sort(listp);
		
		System.out.println("\n---after sort by income");
		for (Product c : listp) 
			System.out.println(c);
		
	}
}