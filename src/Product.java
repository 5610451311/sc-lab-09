

public class Product implements Taxable,Comparable<Product>{
	private String name;
	private double price;
	public Product(String name,double price){
		this.name = name;
		this.price = price;
	}

	
	public double getTax() {
		return price*0.07;
	}
	
	public String toString(){
		return name+" price: "+price;
	}


	@Override
	public int compareTo(Product other) {
		if (this.price < other.price ) { return -1; }
		if (this.price > other.price ) { return 1;  }
		return 0;
	}

}
