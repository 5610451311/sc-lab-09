

import java.util.Comparator;

public class ProfitComparator implements Comparator{
	public int compare(Object o1, Object o2){		
		double com1 = ((Company)o1).getProfit();		
		double com2 = ((Company)o2).getProfit();
		
		if (com1 > com2) {return 1;}		
		if (com1 < com2) {return -1;}		
		return 0;		
	}
}
