

import java.util.Comparator;

public class Company implements Taxable,Comparator<Company>{
	private String companyName;
	private double income;
	private double outcome;
	private double profit;
	
	public Company(String comN, double in, double out){
		companyName = comN;
		income = in;
		outcome = out;
	}
	
	public String getCompanyName(){
		return companyName;
	}
	
	public double getIncome(){
		return income;
	}
	
	public double getOutcome(){
		return outcome;
	}
	
	public double getProfit(){
		return income-outcome;
	}
	
	public double getTax(){
		profit = income-outcome;
		return profit * (0.3);
	}
	
	public int compare(Company o1, Company o2){		
		double c1 = o1.getTax();		
		double c2 = o2.getTax();
		
		if (c1 > c2) {return 1;}		
		if (c1 < c2) {return -1;}		
		return 0;		
	}
	
}
