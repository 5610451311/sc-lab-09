

import java.util.Comparator;

public class EarningComparator implements Comparator{
	
	public int compare(Object o1, Object o2){		
		double com1 = ((Company)o1).getIncome();		
		double com2 = ((Company)o2).getIncome();
		
		if (com1 > com2) {return 1;}		
		if (com1 < com2) {return -1;}		
		return 0;		
	}
	
}
